class Carousel {
    constructor(id, effectItems) {
        this.element = document.getElementById(id);
        this.currentItem = 0;
        this.effectItems = effectItems;
        this.progress = null;
    }

    bindControls() {
        const previousQuote = document.getElementById('previous-quote');
        previousQuote.onclick = this.previous.bind(this);
        const nextQuote = document.getElementById('next-quote');
        nextQuote.onclick = this.next.bind(this);
    }

    start() {
        this.bindControls();
        this.showCurrentItem();
        this.animate();
    }

    next() {
        this.hideLastItem();
        this.currentItem++;
        if (this.currentItem > this.effectItems.length - 1) {
            this.currentItem = 0;
        }
        this.showCurrentItem();
        this.resetProgress();
    }

    previous() {
        this.hideLastItem();
        this.currentItem--;
        if (this.currentItem < 0) {
            this.currentItem = this.effectItems.length - 1;
        }
        this.showCurrentItem();
        this.resetProgress();
    }

    hideLastItem() {
        const itemToHide = this.currentItem;
        setTimeout(() => {
            this.effectItems[itemToHide].style.display = 'none';
        }, 500);
        this.effectItems[this.currentItem].style.opacity = '0';
    }

    showCurrentItem() {
        const itemToHide = this.currentItem;
        setTimeout(() => {
            this.effectItems[itemToHide].style.display = 'block';
        }, 500);
        this.effectItems[this.currentItem].style.opacity = '1';
    }

    resetProgress() {
        this.progress = 0;
        this.animate();
    }

    animate() {
        window.requestAnimationFrame(this.step.bind(this));
    }

    step(timestamp) {
        if (!this.progress) {
            this.progress = timestamp;
        }
        const progress = timestamp - this.progress;
        this.element.style.width = Math.min(progress / 50, 100) + '%';
        if (progress < 5000) {
            this.animate();
        } else {
            this.next();
        }
    }
};

function getCarouselItems() {
    let effectItems = [];
    for (let i = 0; i < 4; i++) {
        effectItems.push(document.getElementById('quote-'+i));
    }
    return effectItems;
}

function activateDropdown(actionElementId, dropdownElementId) {
    const btn = document.getElementById(actionElementId);
    const dropDown = document.getElementById(dropdownElementId);
    btn.onfocus = () => {
        dropDown.style.display = 'block';
    };
    btn.onblur = () => {
        dropDown.style.display = 'none';
    };
    let dropdownElements = dropDown.getElementsByTagName('a');
    for (let i = 0; i < dropdownElements.length; i++) {
        dropdownElements[i].onmousedown = (e) => {
            e.preventDefault();
            dropDown.style.display = 'none';
            btn.blur();
            window.location = dropdownElements[i].href;
        };
    }
}

document.addEventListener("DOMContentLoaded", function () {
    activateDropdown('main-menu', 'main-dropdown');
    activateDropdown('languages-btn', 'languages-dropdown');
    const carousel = new Carousel('quote-progress', getCarouselItems());
    carousel.start();
});
